
package alertas;

import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.DialogPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.StageStyle;

/**
 *
 * Grupo 2 (David Da Silva, Sergio Vazquez, Carlos Perez, David Grande)
 */
public class Alerta {

    public static void showAlert(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initStyle(StageStyle.TRANSPARENT);
        alert.setWidth(200);

        alert.setTitle("Aviso");
        alert.setHeaderText(null);
        alert.setContentText("\t\t\t" + message);

        DialogPane pane = alert.getDialogPane();
        pane.getScene().getWindow().setOpacity(0.8);
        // pane.getStylesheets().add("Stilo1.css");
        pane.setStyle("-fx-background-color: derive( rgba(95, 158, 160, 0.9),60% ); -fx-background-radius: 10; -fx-border-radius: 10; -fx-border-width: 3px; -fx-radius:10;"
                + "-fx-radius:5px;");
        StackPane st = new StackPane(new ImageView(new Image("file:src/resources/img/cancelar.png")));
        //st.setStyle("-fx-background-color: derive(CadetBlue,60%); -fx-background-radious: 10;");
        st.setPrefSize(24, 24);
        st.setAlignment(Pos.CENTER);
        pane.setGraphic(st);
        alert.showAndWait();

    }

    public static void showAlertconforme(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.initStyle(StageStyle.UNDECORATED);

        alert.setTitle("Aviso");
        alert.setHeaderText(null);
        alert.setContentText("\t\t\t" + message);
        DialogPane pane = alert.getDialogPane();
        pane.getScene().getWindow().setOpacity(0.8);
        pane.setStyle("-fx-background-color: derive(CadetBlue,60% ); -fx-background-radius: 10; -fx-border-radius: 8px; -fx-border-width: 3px;"
                + "-fx-radius:5px;");
        StackPane st = new StackPane(new ImageView(new Image("file:src/resources/img/check.png")));
        st.setStyle("-fx-background-color: null");

        st.setPrefSize(24, 24);
        st.setAlignment(Pos.CENTER);
        pane.setGraphic(st);
        alert.showAndWait();

    }

}
