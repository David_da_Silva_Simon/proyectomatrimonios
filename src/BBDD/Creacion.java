
package BBDD;

import java.sql.*;

/**
 *
 * @author Grupo 2 (David Da Silva, Sergio Vazquez, Carlos Perez, David Grande)
 */
public class Creacion {

    public static void creamosbbdd() {

        try {

            Connection conexion = DriverManager.getConnection("jdbc:mysql://localhost:3307/?user=root&password=usbw");
            Statement sentencia = conexion.createStatement();

            sentencia.execute("CREATE DATABASE IF NOT EXISTS agencia_matrimonial;");
            sentencia.execute("USE agencia_matrimonial;");

            sentencia.execute("create table if not exists personas(\n"
                    + "  dni char(9) not null,\n"
                    + "  nombre varchar(30) not null,\n"
                    + "  genero enum('hombre','mujer') not null default 'hombre',\n"
                    + "  mail varchar(50) not null,\n"
                    + "  telf varchar(14) not null,\n"
                    + "  direccion varchar(30) not null,\n"
                    + "  edad int(3) unsigned not null,\n"
                    + "  sexualidad enum('heterosexual','homosexual','bisexual') not null default 'heterosexual',\n"
                    + "  preferencias text null,\n"
                    + "  casado boolean,\n"
                    + "  foto longblob null,\n"
                    + "  primary key(dni)\n"
                    + ")engine innodb;");

            sentencia.execute("create table if not exists citas(\n"
                    + "  persona1 char(9) not null,\n"
                    + "  persona2 char(9) not null,\n"
                    + "  fecha date not null,\n"
                    + "  lugar varchar(20) not null,\n"
                    + "  lugaralt double not null,\n"
                    + "  lugarlat double not null,\n"
                    + "  primary key(persona1,persona2,fecha),\n"
                    + "  foreign key (persona1) references personas(dni) on delete cascade on update cascade,\n"
                    + "  index fk1_personas (persona1),\n"
                    + "  foreign key (persona2) references personas(dni) on delete cascade on update cascade,\n"
                    + "  index fk2_personas (persona2)\n"
                    + ")engine innodb;");

            sentencia.execute("create table if not exists matrimonios(\n"
                    + "  persona1 char(9) not null,\n"
                    + "  persona2 char(9) not null,\n"
                    + "  fecha date not null,\n"
                    + "  primary key(persona1,persona2),\n"
                    + "  foreign key (persona1) references personas(dni) on delete cascade on update cascade,\n"
                    + "  index fk3_personas (persona1),\n"
                    + "  foreign key (persona2) references personas(dni) on delete cascade on update cascade,\n"
                    + "  index fk4_personas (persona2)\n"
                    + ")engine innodb;");

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }
}
