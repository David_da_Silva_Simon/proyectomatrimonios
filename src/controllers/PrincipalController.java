
package controllers;

import BBDD.Creacion;
import com.jfoenix.controls.JFXButton;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import proyectomat.NewHibernateUtil;

/**
 *
 * @author Grupo 2 (David Da Silva, Sergio Vazquez, Carlos Perez, David Grande)
 */
public class PrincipalController {

    @FXML
    private JFXButton btncerrarprin;

    @FXML
    private JFXButton btnminiprin;

    @FXML
    private JFXButton btnentrar;

    private double xOffset, yOffset;

    @FXML
    void entrar(ActionEvent event) throws Exception {
        final URL url = new File("src/resources/Secundaria.fxml").toURL();

        Task<Void> sleeper = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                try {
                    Thread.sleep(400);
                } catch (InterruptedException e) {
                }
                return null;
            }
        };
        sleeper.setOnSucceeded(new EventHandler<WorkerStateEvent>() {
            @Override
            public void handle(WorkerStateEvent evt) {
                try {
                    ResourceBundle resources = ResourceBundle.getBundle("bundles/Matrimonios",
                            new Locale(System.getProperty("user.language"), System.getProperty("user.country")));
                    if (resources == null) {
                        resources = ResourceBundle.getBundle("bundles/Matrimonios", new Locale("es"));
                    }
                    Parent root = FXMLLoader.load(url, resources);
                    //Parent root = FXMLLoader.load(getClass().getResource("Secundaria.fxml"));
                    Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                    Scene scene = new Scene(root);

                    root.setOnMousePressed(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            xOffset = event.getSceneX();
                            yOffset = event.getSceneY();
                        }
                    });
                    root.setOnMouseDragged(new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            stage.setX(event.getScreenX() - xOffset);
                            stage.setY(event.getScreenY() - yOffset);
                        }
                    });
                    stage.setScene(scene);
                    stage.toFront();
                    stage.show();
                } catch (IOException e) {
                    System.out.println(e);
                }
            }
        });
        new Thread(sleeper).start();
    }

    @FXML
    void cerrar(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();
        System.exit(0);
    }

    @FXML
    void minimizar(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setIconified(true);
    }

    @FXML
    void initialize() {
        Creacion.creamosbbdd();

        NewHibernateUtil.getSessionFactory();

    }

}
