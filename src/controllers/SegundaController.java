package controllers;


import Pojos.Cita;
import Pojos.Genero;
import Pojos.Matrimonio;
import Pojos.Persona;
import alertas.Alerta;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXTreeTableView;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
/**
 *
 * Grupo 2 (David Da Silva, Sergio Vazquez, Carlos Perez, David Grande)
 */

import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.Toggle;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.Effect;
import javafx.scene.image.Image;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import mapas.Llamada;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import proyectomat.NewHibernateUtil;
import utilidadopenstreetmap.OpenStreetMapUtils;
import validaciones.ValidarDNI;
import validaciones.ValidarEmail;

public class SegundaController {

    @FXML
    private Tab tab1;

    @FXML
    private JFXTextField txtnombre;

    @FXML
    private JFXTextField txtdni;

    @FXML
    private RadioButton rdbtnhetero;

    @FXML
    private ToggleGroup Sexualidad;

    @FXML
    private RadioButton rdbtnhomo;

    @FXML
    private RadioButton rdbtnbi;

    @FXML
    private TextArea txtpref;

    @FXML
    private JFXTextField txtmail;

    @FXML
    private JFXTextField txttlf;

    @FXML
    private JFXTextField txtdireccion;

    @FXML
    private Button btnselecimg;

    @FXML
    private ImageView lblfoto;

    @FXML
    private TextField lbldirimg;

    @FXML
    private JFXButton btnalta;

    @FXML
    private RadioButton rdbtnhombre;

    @FXML
    private ToggleGroup sexo;

    @FXML
    private RadioButton rdbtnmujer;

    @FXML
    private JFXComboBox<String> cbmedadalta;

    @FXML
    private JFXTreeTableView<Persona> tablabajas;

    @FXML
    private TreeTableColumn<Persona, String> bjdni;

    @FXML
    private TreeTableColumn<Persona, String> bjnombre;

    @FXML
    private TreeTableColumn<Persona, String> bjtlf;

    @FXML
    private TreeTableColumn<Persona, String> bjemail;

    @FXML
    private TreeTableColumn<Persona, String> bjgenero;

    @FXML
    private TreeTableColumn<Persona, String> bjedad;

    @FXML
    private TreeTableColumn<Persona, String> bjdire;

    @FXML
    private TreeTableColumn<Persona, String> bjsexo;

    @FXML
    private JFXButton btnborrar;

    @FXML
    private JFXTextField txtbuscdni;

    @FXML
    private JFXTreeTableView<Persona> tablabajas1;

    @FXML
    private TreeTableColumn<Persona, String> mddni;

    @FXML
    private TreeTableColumn<Persona, String> mdnombre;

    @FXML
    private TreeTableColumn<Persona, String> mdtlf;

    @FXML
    private TreeTableColumn<Persona, String> mdmeil;

    @FXML
    private TreeTableColumn<Persona, String> mdgen;

    @FXML
    private TreeTableColumn<Persona, String> mdedad;

    @FXML
    private TreeTableColumn<Persona, String> mddire;

    @FXML
    private TreeTableColumn<Persona, String> mdsexo;

    @FXML
    private JFXTextField txtnombremod;

    @FXML
    private JFXTextField txtmailmod;

    @FXML
    private JFXTextField txttlfmod;

    @FXML
    private JFXTextField txtdireccionmod;

    @FXML
    private JFXButton btnmodificar;

    @FXML
    private JFXComboBox<String> cmbedadmod;

    @FXML
    private AnchorPane cbmpersona2;

    @FXML
    private JFXComboBox<Persona> cmbpersona1;

    @FXML
    private JFXComboBox<Persona> cmbpersona2;

    @FXML
    private JFXButton mapsview;

    @FXML
    private JFXTextField txtlugar;

    @FXML
    private JFXTextField txtfecha;

    @FXML
    private JFXButton btnemail;

    @FXML
    private Label lblmsotrarcita;

    @FXML
    private JFXButton btncrearcita;

    @FXML
    private JFXButton btncalendar;

    @FXML
    private JFXButton btncorazon;

    @FXML
    private ImageView img1;

    @FXML
    private ImageView img2;

    @FXML
    private JFXComboBox<Persona> cmbpersona21;

    @FXML
    private JFXComboBox<Persona> cmbpersona22;

    @FXML
    private JFXButton btncalendar2;

    @FXML
    private JFXTreeTableView<Persona> tablabajas2;

    @FXML
    private TreeTableColumn<Persona, String> cclidni;

    @FXML
    private TreeTableColumn<Persona, String> cclinom;

    @FXML
    private TreeTableColumn<Persona, String> cclitlf;

    @FXML
    private TreeTableColumn<Persona, String> cclimeil;

    @FXML
    private TreeTableColumn<Persona, String> ccligen;

    @FXML
    private TreeTableColumn<Persona, String> ccliedad;

    @FXML
    private TreeTableColumn<Persona, String> cclidire;

    @FXML
    private TreeTableColumn<Persona, String> cclisexo;

    @FXML
    private JFXButton btnMod1;

    @FXML
    private JFXTextField txtbuscli;

    @FXML
    private JFXTreeTableView<Cita> tablabajas21;

    @FXML
    private TreeTableColumn<Cita, String> citasp1;

    @FXML
    private TreeTableColumn<Cita, String> citasp2;

    @FXML
    private TreeTableColumn<Cita, String> citaslugar;

    @FXML
    private TreeTableColumn<Cita, String> citasfecha;

    @FXML
    private JFXButton btnMod2;

    @FXML
    private JFXTextField txtbuscita;

    @FXML
    private JFXTreeTableView<Matrimonio> tablabajas22;

    @FXML
    private TreeTableColumn<Matrimonio, String> matp1;

    @FXML
    private TreeTableColumn<Matrimonio, String> matp2;

    @FXML
    private TreeTableColumn<Matrimonio, String> matfecha;

    @FXML
    private JFXButton btnMod3;

    @FXML
    private JFXTextField txtbusbod;

    @FXML
    private JFXButton btnSalir;

    @FXML
    private JFXDatePicker fechaCita;

    @FXML
    private JFXDatePicker fechaBoda;

    @FXML
    private JFXComboBox<String> cmbsexoconsulcli;

    private double lon, lat;
    private static final double BLUR_AMOUNT = 2;

    private static final Effect frostEffect
            = new BoxBlur(BLUR_AMOUNT, BLUR_AMOUNT, 10);

    private ResourceBundle bundle;

    @FXML
    void cerrar(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();
        NewHibernateUtil.getSessionFactory().close();
        System.exit(0);
    }

    @FXML
    void alta(ActionEvent event) {

        System.out.println(bundle.getBaseBundleName());
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        try {
            String nombre = txtnombre.getText();
            String dni = txtdni.getText().toUpperCase();
            if (ValidarDNI.comprobar(dni) == 1) {
                String email = txtmail.getText();
                if (ValidarEmail.validarmail(email) == 0) {
                    String telf = txttlf.getText();
                    String direccion = txtdireccion.getText();
                    byte[] foto = null;

                    if (!lbldirimg.getText().equals("")) {
                        foto = Files.readAllBytes(selectedFile.toPath());
                    }
                    RadioButton rbG = (RadioButton) sexo.getSelectedToggle();
                    Genero genero = (rbG.getText().equals(bundle.getString("genderM"))) ? Genero.hombre : Genero.mujer;
                    RadioButton rbS = (RadioButton) Sexualidad.getSelectedToggle();
                    Pojos.Sexualidad sexu = (rbS.getText().equals("Heterosexual"))
                            ? Pojos.Sexualidad.heterosexual : ((rbS.getText().equals("Homosexual")) ? Pojos.Sexualidad.homosexual : Pojos.Sexualidad.bisexual);
                    int edad = Integer.parseInt(cbmedadalta.getValue());
                    if (edad >= 18 && edad < 110) {

                        String pref = txtpref.getText();;

                        Persona p = new Persona(nombre, dni, email, telf, direccion, genero, sexu, edad);
                        if (foto != null) {
                            p.setFoto(foto);
                        } else {
                            File f = new File("src/resources/img/img_nodisp.png");
                            foto = Files.readAllBytes(f.toPath());
                            p.setFoto(foto);
                        }
                        if (!pref.equals("")) {
                            p.setPreferencias(pref);
                        }
                        int errorBD = guardarModificar(p);
                        if (errorBD != -1) {
                            limpiarCampos();
                            anyadirPersonas(p);
                        }
                    } else {
                        stage.getScene().lookup("#panelFondo").setEffect(frostEffect);
                        Alerta.showAlert(bundle.getString("ageerr"));
                        stage.getScene().lookup("#panelFondo").setEffect(null);
                    }
                } else {
                    stage.getScene().lookup("#panelFondo").setEffect(frostEffect);
                    Alerta.showAlert(bundle.getString("mailerr"));
                    stage.getScene().lookup("#panelFondo").setEffect(null);
                }
            } else {
                stage.getScene().lookup("#panelFondo").setEffect(frostEffect);
                Alerta.showAlert(bundle.getString("iderr"));
                stage.getScene().lookup("#panelFondo").setEffect(null);

            }

        } catch (IOException e) {
            System.out.println("Error cargando la foto");
        }

        // RadioButton rb = (RadioButton) sexo.getSelectedToggle();
        // System.out.println(rb.getText());
    }

    @FXML
    void baja(ActionEvent event) {
        // codigo para borrar, habria que asegurarse de borrar tambien la entrada en la tabla de modificaciones
        TreeItem<Persona> sel = tablabajas.getSelectionModel().getSelectedItem();
        Persona p = null;
        if (sel != null) {
            //   System.out.println(sel.getValue().getDni());

            p = sel.getValue();
            eliminar(p);
            tablabajas.getRoot().getChildren().remove(sel);
            tablabajas1.getRoot().getChildren().remove(sel);
            tablabajas2.getRoot().getChildren().remove(sel);
            cmbpersona1.getItems().remove(p);
            cmbpersona21.getItems().remove(p);
            cmbpersona1.getSelectionModel().select(-1);
            cmbpersona21.getSelectionModel().select(-1);
            Persona borr = null;
            for (Persona ps : cmbpersona2.getItems()) {
                if (ps.getDni().equals(p.getDni())) {
                    borr = ps;
                }
            }
            if (borr != null) {
                cmbpersona2.getItems().remove(borr);
            }
            for (Persona ps : cmbpersona22.getItems()) {
                if (ps.getDni().equals(p.getDni())) {
                    borr = ps;
                }
            }
            if (borr != null) {
                cmbpersona22.getItems().remove(borr);
            }

            tablabajas21.getRoot().getChildren().clear();
            tablabajas22.getRoot().getChildren().clear();
            iniCitas();
            iniMat();
        }

    }

    @FXML
    void modificar(ActionEvent event) {
        /* Persona per;
        
        TreeItem<Persona> sel = tablabajas1.getSelectionModel().getSelectedItem();
        int edad=Integer.parseInt(cmbedadmod.getValue());
        Genero gen=sel.getValue().getGenero();
        Pojos.Sexualidad sexo=sel.getValue().getSexualidad();
        per=new Persona(txtnombremod.getText(),sel.getValue().getDni() , txtmailmod.getText(), txttlfmod.getText(),txtdireccionmod.getText(),gen ,sexo ,edad );
        guardarModificar(per);
        TreeItem<Persona> child = new TreeItem<>(per);
        tablabajas1.getRoot().getChildren().remove(sel);
        tablabajas1.getRoot().getChildren().add(child);*/

        // mejor asi
        TreeItem<Persona> sel = tablabajas1.getSelectionModel().getSelectedItem();
        sel.getValue().setEdad(Integer.parseInt(cmbedadmod.getValue()));
        sel.getValue().setNombre(txtnombremod.getText());
        sel.getValue().setTelf(txttlfmod.getText());
        sel.getValue().setDireccion(txtdireccionmod.getText());
        sel.getValue().setMail(txtmailmod.getText());
        tablabajas1.refresh();
        tablabajas.refresh();
        tablabajas2.refresh();
        guardarModificar(sel.getValue());
        //  sel.getValue()

    }

    @FXML
    void selecmod(MouseEvent event) {
        TreeItem<Persona> sel = tablabajas1.getSelectionModel().getSelectedItem();
        Persona p = sel.getValue();
        txtnombremod.setText(p.getNombre());
        txttlfmod.setText(p.getTelf());
        txtdireccionmod.setText(p.getDireccion());
        txtmailmod.setText(p.getMail());
        cmbedadmod.setValue(String.valueOf(p.getEdad()));
    }

    private File selectedFile;

    @FXML
    void selectImg(ActionEvent event) {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Imagenes", "*.jpg", "*.png", "*.gif")
        );
        try {
            selectedFile = fileChooser.showOpenDialog(stage);
            String filepath = selectedFile.getAbsolutePath();
            lbldirimg.setText(filepath);
            String url = "file:" + selectedFile.getAbsolutePath();
            System.out.println(selectedFile.getAbsolutePath());
            lblfoto.setImage(new Image(url));
        } catch (Exception e) {

        }
    }

    @FXML
    void mayus(KeyEvent event) {
        if (txtdni.getText().length() == 9) {
            txtdni.setText(txtdni.getText().toUpperCase());
        }
    }

    @FXML
    void initialize() {
        //bundle = resources;
        //   tab1.setText(bundle.getString("tab1"));
        bundle = ResourceBundle.getBundle("bundles/Matrimonios",
                new Locale(System.getProperty("user.language"), System.getProperty("user.country")));
        if (bundle == null) {
            bundle = ResourceBundle.getBundle("bundles/Matrimonios", new Locale("es"));
        }
        cbmedadalta.getItems().clear();
        cbmedadalta.getItems().addAll("20", "30", "40", "50", "60");
        cbmedadalta.getSelectionModel().select(0);
        cmbedadmod.getItems().clear();
        cmbedadmod.getItems().addAll("20", "30", "40", "50", "60");
        cmbedadmod.getSelectionModel().select(0);
        cmbpersona1.setCellFactory(cellFactory);
        cmbpersona1.setButtonCell(cellFactory.call(null));
        cmbpersona2.setCellFactory(cellFactory);
        cmbpersona2.setButtonCell(cellFactory.call(null));
        cmbpersona21.setCellFactory(cellFactory);
        cmbpersona21.setButtonCell(cellFactory.call(null));
        cmbpersona22.setCellFactory(cellFactory);
        cmbpersona22.setButtonCell(cellFactory.call(null));
        Tooltip t = new Tooltip();
        t.setText(bundle.getString("dbclk"));
        tablabajas21.setTooltip(t);
        Persona p = new Persona();
        Cita c = new Cita();
        Matrimonio m = new Matrimonio();
        TreeItem<Persona> root = new TreeItem<>(p);
        TreeItem<Persona> root1 = new TreeItem<>(p);
        TreeItem<Persona> root2 = new TreeItem<>(p);
        TreeItem<Cita> root3 = new TreeItem<>(c);
        TreeItem<Matrimonio> root4 = new TreeItem<>(m);
        root.setExpanded(true);

        tablabajas.setShowRoot(false);
        tablabajas.setRoot(root);
        tablabajas1.setShowRoot(false);
        tablabajas1.setRoot(root1);
        tablabajas2.setShowRoot(false);
        tablabajas2.setRoot(root2);
        tablabajas21.setShowRoot(false);
        tablabajas21.setRoot(root3);
        tablabajas22.setShowRoot(false);
        tablabajas22.setRoot(root4);
        initCol();
        //con estas lambdas inicializamos las columnas para que cojan el valor de los TreeItem hijos

        cmbsexoconsulcli.getItems().clear();
        cmbsexoconsulcli.getItems().addAll(bundle.getString("all"), "bisexual", "heterosexual", "homosexual");
        cmbsexoconsulcli.getSelectionModel().select(-1);
        iniPersonas();
        iniCitas();
        iniMat();

    }

    //inicialización de las columnas de cada tabla para que se llenen automaticamente
    public void initCol() {
        System.out.println("a");
        /*columnas de la tabla de bajas*/
        bjdni.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getDni()));
        bjnombre.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getNombre()));
        bjtlf.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getTelf()));
        bjemail.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getMail()));
        bjdire.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getDireccion()));
        bjedad.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getValue().getEdad())));
        /*bjgenero.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getValue().getGenero())));*/
        bjgenero.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(((p.getValue().getValue().getGenero() == Genero.hombre)
                        ? bundle.getString("genderM") : bundle.getString("genderF"))));
        bjsexo.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getValue().getSexualidad())));
        System.out.println("b");
        /*columnas de la tabla de modificaciones*/
        mddni.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getDni()));
        mdnombre.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getNombre()));
        mdtlf.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getTelf()));
        mdmeil.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getMail()));
        mddire.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getDireccion()));
        mdedad.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getValue().getEdad())));
        /*mdgen.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getValue().getGenero())));*/
        mdgen.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(((p.getValue().getValue().getGenero() == Genero.hombre)
                        ? bundle.getString("genderM") : bundle.getString("genderF"))));
        mdsexo.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getValue().getSexualidad())));

        /*columnas de la tabla de consultas de usuarios*/
        cclidni.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getDni()));
        cclinom.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getNombre()));
        cclitlf.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getTelf()));
        cclimeil.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getMail()));
        cclidire.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getDireccion()));
        ccliedad.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getValue().getEdad())));
        /*ccligen.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getValue().getGenero())));*/
        ccligen.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(((p.getValue().getValue().getGenero() == Genero.hombre)
                        ? bundle.getString("genderM") : bundle.getString("genderF"))));
        cclisexo.setCellValueFactory((TreeTableColumn.CellDataFeatures<Persona, String> p)
                -> new ReadOnlyStringWrapper(String.valueOf(p.getValue().getValue().getSexualidad())));

        /*columnas de la tabla citas*/
        citasp1.setCellValueFactory((TreeTableColumn.CellDataFeatures<Cita, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getPersona1().getDni()));
        citasp2.setCellValueFactory((TreeTableColumn.CellDataFeatures<Cita, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getPersona2().getDni()));
        citaslugar.setCellValueFactory((TreeTableColumn.CellDataFeatures<Cita, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getNombreLugar()));
        citasfecha.setCellValueFactory((TreeTableColumn.CellDataFeatures<Cita, String> p)
                -> new ReadOnlyStringWrapper(new SimpleDateFormat("dd-MM-yy").format(p.getValue().getValue().getFecha())));

        matp1.setCellValueFactory((TreeTableColumn.CellDataFeatures<Matrimonio, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getPersona1().getDni()));
        matp2.setCellValueFactory((TreeTableColumn.CellDataFeatures<Matrimonio, String> p)
                -> new ReadOnlyStringWrapper(p.getValue().getValue().getPersona2().getDni()));
        matfecha.setCellValueFactory((TreeTableColumn.CellDataFeatures<Matrimonio, String> p)
                -> new ReadOnlyStringWrapper(new SimpleDateFormat("dd-MM-yy").format(p.getValue().getValue().getFecha())));

    }

    public void limpiarCampos() {
        txtnombre.setText("");
        txtdni.setText("");
        txtmail.setText("");
        txttlf.setText("");
        txtdireccion.setText("");
        lbldirimg.setText("");

        lblfoto.setImage(null);
        txtpref.setText("");
        sexo.selectToggle((Toggle) rdbtnhombre);
        Sexualidad.selectToggle((Toggle) rdbtnhetero);
    }

    /*esto va a ir en otra clase aparte*/
    public static int guardarModificar(Object objeto) {
        Session sesion;
        try {
            sesion = NewHibernateUtil.getSession();
            sesion.beginTransaction();
            sesion.saveOrUpdate(objeto);
            sesion.getTransaction().commit();
            sesion.close();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
            return -1;
        }
        return 0;
    }

    public static void eliminar(Object objeto) {
        Session sesion;
        try {
            sesion = NewHibernateUtil.getSession();
            sesion.beginTransaction();
            sesion.delete(objeto);
            sesion.getTransaction().commit();
            sesion.close();
        } catch (HibernateException e) {
            System.out.println(e.getMessage());
        }
    }

    // inicializa las tablas
    public void iniPersonas() {

        Session s = NewHibernateUtil.getSession();
        List<Object> personas = s.createCriteria(Persona.class).list();
        for (Object o : personas) {
            anyadirPersonas((Persona) o);
        }
        s.close();
    }

    public void iniCitas() {
        Session s = NewHibernateUtil.getSession();
        List<Object> citas = s.createCriteria(Cita.class).list();
        for (Object o : citas) {
            anyadirCitas((Cita) o);
        }
        s.close();
    }

    public void iniMat() {
        Session s = NewHibernateUtil.getSession();
        List<Object> matrimonios = s.createCriteria(Matrimonio.class).list();
        for (Object o : matrimonios) {
            anyadirMat((Matrimonio) o);
        }
        s.close();
    }

    //llena
    public void iniPersonas(String filtro) {
        Session s = NewHibernateUtil.getSession();
        List<Object> personas = s.createCriteria(Persona.class).list();
        for (Object o : personas) {
            if (((Persona) o).getDni().toLowerCase().matches(".*" + filtro + ".*")) {
                anyadirPersonasF((Persona) o);
            }
        }
        s.close();
    }

    public void iniPersonasC(String filtro) {
        Session s = NewHibernateUtil.getSession();
        List<Object> personas = s.createCriteria(Persona.class).list();
        for (Object o : personas) {
            if (((Persona) o).getDni().toLowerCase().matches(".*" + filtro + ".*")) {
                anyadirPersonasC((Persona) o);
            }
        }
        s.close();
    }

    // con este metodo añadimos los valores al treetableview
    public void anyadirPersonas(Persona p) {
        TreeItem<Persona> child = new TreeItem<>(p);
        tablabajas.getRoot().getChildren().add(child);
        tablabajas1.getRoot().getChildren().add(child);
        tablabajas2.getRoot().getChildren().add(child);
        if (!p.isCasado()) {
            cmbpersona1.getItems().add(p);
            cmbpersona21.getItems().add(p);
        }
    }

    public void iniPersonasSexo(String filtro) {
        Session s = NewHibernateUtil.getSession();
        List<Object> personas = s.createCriteria(Persona.class).list();
        for (Object o : personas) {
            if ((String.valueOf(((Persona) o).getSexualidad())).equalsIgnoreCase(filtro)) {
                anyadirPersonasC((Persona) o);
            }
        }
        s.close();
    }

    public void anyadirCitas(Cita c) {
        TreeItem<Cita> child = new TreeItem<>(c);
        tablabajas21.getRoot().getChildren().add(child);
    }

    public void anyadirMat(Matrimonio m) {
        TreeItem<Matrimonio> child = new TreeItem<>(m);
        System.out.println(m.getPersona1().getDni());
        tablabajas22.getRoot().getChildren().add(child);
    }

    //con este metodo filtro la tabla de bajas
    public void anyadirPersonasF(Persona p) {
        TreeItem<Persona> child = new TreeItem<>(p);
        tablabajas.getRoot().getChildren().add(child);

    }

    public void anyadirPersonasC(Persona p) {
        TreeItem<Persona> child = new TreeItem<>(p);
        tablabajas2.getRoot().getChildren().add(child);

    }

    //filtrar dni
    @FXML
    void dnicambiado(KeyEvent event) {
        String filtro = txtbuscdni.getText().toLowerCase().toUpperCase();
        System.out.println(filtro);

        if (!filtro.equals("") & !filtro.matches("[+,-,*,/]*")) {
            tablabajas.getRoot().getChildren().clear();

            iniPersonas(filtro);
        } else {
            tablabajas.getRoot().getChildren().clear();
            tablabajas1.getRoot().getChildren().clear();
            tablabajas2.getRoot().getChildren().clear();
            cmbpersona1.getItems().clear();
            cmbpersona21.getItems().clear();
            iniPersonas();
        }
    }

    @FXML
    void dniconsultas(KeyEvent event) {
        String filtro = txtbuscli.getText().toLowerCase();
        System.out.println(filtro);

        if (!filtro.equals("") & !filtro.matches("[+,-,*,/]*")) {
            tablabajas2.getRoot().getChildren().clear();

            iniPersonasC(filtro);
        } else {
            tablabajas.getRoot().getChildren().clear();
            tablabajas1.getRoot().getChildren().clear();
            tablabajas2.getRoot().getChildren().clear();
            cmbpersona1.getItems().clear();
            cmbpersona21.getItems().clear();
            iniPersonas();
        }
    }

    // con esto guardamos las coordenadas si las encontramos, (no es perfecto), si no nos avisa de que no encuentra
    // el lugar deseado
    public int getGeocode() {
        String dir = txtlugar.getText();

        //    System.out.println(dir);
        try {
            Map<String, Double> coords;
            coords = OpenStreetMapUtils.getInstance().getCoordinates(dir);

            lon = coords.get("lon");
            lat = coords.get("lat");
            //Llamada.verMapa(dir, lat, lon);
        } catch (NullPointerException e) {
            Stage stage = (Stage) txtlugar.getScene().getWindow();
            stage.getScene().lookup("#panelFondo").setEffect(frostEffect);
            Alerta.showAlert(bundle.getString("plerr"));
            stage.getScene().lookup("#panelFondo").setEffect(null);
            return 1;
        }
        return 0;
    }

    Callback<ListView<Persona>, ListCell<Persona>> cellFactory = new Callback<ListView<Persona>, ListCell<Persona>>() {
        @Override
        public ListCell<Persona> call(ListView<Persona> l) {
            return new ListCell<Persona>() {
                @Override
                protected void updateItem(Persona item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setGraphic(null);
                    } else {
                        setText(item.getDni() + "-" + item.getNombre());
                    }
                }
            };
        }
    };

    public int filtroParejas(Persona p, Genero g, Pojos.Sexualidad s) {
        switch (g) {
            case hombre:
                switch (s) {
                    case heterosexual:
                        if (p.getGenero() == Genero.mujer
                                && (p.getSexualidad() == Pojos.Sexualidad.heterosexual
                                || p.getSexualidad() == Pojos.Sexualidad.bisexual)) {
                            cmbpersona2.getItems().add(p);
                            return 1;
                        }
                        break;

                    case homosexual:
                        if (p.getGenero() == Genero.hombre) {
                            if (p.getSexualidad() == Pojos.Sexualidad.homosexual
                                    || p.getSexualidad() == Pojos.Sexualidad.bisexual) {
                                cmbpersona2.getItems().add(p);
                                return 1;

                            }
                        }
                        break;
                    case bisexual:
                        if ((p.getGenero() == Genero.hombre && p.getSexualidad() == Pojos.Sexualidad.heterosexual)
                                || (p.getGenero() == Genero.mujer && p.getSexualidad() == Pojos.Sexualidad.homosexual)) {

                        } else {
                            cmbpersona2.getItems().add(p);
                            return 1;
                        }
                        break;
                }
                break;
            case mujer:
                switch (s) {
                    case heterosexual:
                        if (p.getGenero() == Genero.hombre
                                && (p.getSexualidad() == Pojos.Sexualidad.heterosexual
                                || p.getSexualidad() == Pojos.Sexualidad.bisexual)) {
                            cmbpersona2.getItems().add(p);
                            return 1;
                        }
                        break;

                    case homosexual:
                        if (p.getGenero() == Genero.mujer) {
                            if (p.getSexualidad() == Pojos.Sexualidad.homosexual
                                    || p.getSexualidad() == Pojos.Sexualidad.bisexual) {
                                cmbpersona2.getItems().add(p);
                                return 1;

                            }
                        }
                        break;
                    case bisexual:
                        if ((p.getGenero() == Genero.mujer && p.getSexualidad() == Pojos.Sexualidad.heterosexual)
                                || (p.getGenero() == Genero.hombre && p.getSexualidad() == Pojos.Sexualidad.homosexual)) {

                        } else {
                            cmbpersona2.getItems().add(p);
                            return 1;
                        }
                        break;
                }
                break;
        }
        return 0;
    }

    public int filtroParejas2(Persona p, Genero g, Pojos.Sexualidad s) {
        switch (g) {
            case hombre:
                switch (s) {
                    case heterosexual:
                        if (p.getGenero() == Genero.mujer
                                && (p.getSexualidad() == Pojos.Sexualidad.heterosexual
                                || p.getSexualidad() == Pojos.Sexualidad.bisexual)) {
                            cmbpersona22.getItems().add(p);
                            return 1;
                        }
                        break;

                    case homosexual:
                        if (p.getGenero() == Genero.hombre) {
                            if (p.getSexualidad() == Pojos.Sexualidad.homosexual
                                    || p.getSexualidad() == Pojos.Sexualidad.bisexual) {
                                cmbpersona22.getItems().add(p);
                                return 1;

                            }
                        }
                        break;
                    case bisexual:
                        if ((p.getGenero() == Genero.hombre && p.getSexualidad() == Pojos.Sexualidad.heterosexual)
                                || (p.getGenero() == Genero.mujer && p.getSexualidad() == Pojos.Sexualidad.homosexual)) {

                        } else {
                            cmbpersona22.getItems().add(p);
                            return 1;
                        }
                        break;
                }
                break;
            case mujer:
                switch (s) {
                    case heterosexual:
                        if (p.getGenero() == Genero.hombre
                                && (p.getSexualidad() == Pojos.Sexualidad.heterosexual
                                || p.getSexualidad() == Pojos.Sexualidad.bisexual)) {
                            cmbpersona22.getItems().add(p);
                            return 1;
                        }
                        break;

                    case homosexual:
                        if (p.getGenero() == Genero.mujer) {
                            if (p.getSexualidad() == Pojos.Sexualidad.homosexual
                                    || p.getSexualidad() == Pojos.Sexualidad.bisexual) {
                                cmbpersona22.getItems().add(p);
                                return 1;

                            }
                        }
                        break;
                    case bisexual:
                        if ((p.getGenero() == Genero.mujer && p.getSexualidad() == Pojos.Sexualidad.heterosexual)
                                || (p.getGenero() == Genero.hombre && p.getSexualidad() == Pojos.Sexualidad.homosexual)) {

                        } else {
                            cmbpersona22.getItems().add(p);
                            return 1;
                        }
                        break;
                }
                break;
        }
        return 0;
    }

    @FXML
    void cambioPersonaCita(ActionEvent event) {
        Persona p = cmbpersona1.getSelectionModel().getSelectedItem();
        if (p != null) {
            cmbpersona2.getItems().clear();
            listaComboP2(p);
            /*   if (p.getFoto() != null) {
                Image i = new Image(new ByteArrayInputStream(p.getFoto()));

                img1.setImage(i);
            } else {
                img1.setImage(null);
            }*/
        }
    }

    public void listaComboP2(Persona p) {
        Session s = NewHibernateUtil.getSession();
        List<Object> personas = s.createCriteria(Persona.class).list();
        int modificado = 0;
        for (Object o : personas) {
            if (!p.getDni().equals(((Persona) o).getDni())) {
                if (!((Persona) o).isCasado()) {
                    modificado += filtroParejas((Persona) o, p.getGenero(), p.getSexualidad());
                }
            }
        }
        if (modificado != 0) {
            cmbpersona2.setDisable(false);
        } else {
            cmbpersona2.setDisable(true);
            Stage stage = (Stage) cmbpersona2.getScene().getWindow();
            stage.getScene().lookup("#panelFondo").setEffect(frostEffect);
            Alerta.showAlert(bundle.getString("pererr"));
            stage.getScene().lookup("#panelFondo").setEffect(null);
        }
        s.close();
    }

    public void listaComboP22(Persona p) {
        Session s = NewHibernateUtil.getSession();
        List<Object> personas = s.createCriteria(Persona.class).list();
        int modificado = 0;
        for (Object o : personas) {
            if (!p.getDni().equals(((Persona) o).getDni())) {
                if (!((Persona) o).isCasado()) {
                    modificado += filtroParejas2((Persona) o, p.getGenero(), p.getSexualidad());
                }
            }
        }
        if (modificado != 0) {
            cmbpersona22.setDisable(false);
        } else {
            cmbpersona22.setDisable(true);
            Stage stage = (Stage) cmbpersona2.getScene().getWindow();
            stage.getScene().lookup("#panelFondo").setEffect(frostEffect);
            Alerta.showAlert(bundle.getString("pererr"));
            stage.getScene().lookup("#panelFondo").setEffect(null);
        }
        s.close();
    }

    @FXML
    void altaCita(ActionEvent event) throws ParseException {
        int error = getGeocode();
        if (error != 1) {
            Session s = NewHibernateUtil.getSession();

            String fecha = String.valueOf(fechaCita.getValue());

            System.out.println(fecha);

            Persona p1 = (Persona) s.get(Persona.class, cmbpersona1.getSelectionModel().getSelectedItem().getDni());
            Persona p2 = (Persona) s.get(Persona.class, cmbpersona2.getSelectionModel().getSelectedItem().getDni());
            String lugar = txtlugar.getText();
            Cita c = new Cita(p1, p2, new SimpleDateFormat("yyyy-MM-dd").parse(fechaCita.getValue().toString()), lugar, lat, lon);

            lblmsotrarcita.setText(p1.getNombre() + " " + bundle.getString("dateinfo1") + p2.getNombre() + " " + bundle.getString("dateinfo2") + " "
                    + lugar + " " + bundle.getString("dateinfo3") + " " + new SimpleDateFormat("dd-MM-yy").format(c.getFecha()));

            p1.getCitasP1().add(c);
            p2.getCitasP2().add(c);
            s.close();
            guardarModificar(c);
            guardarModificar(p1);
            guardarModificar(p2);
            anyadirCitas(c);
            clearCitas();

        }
    }

    public void clearCitas() {
        cmbpersona2.getItems().clear();
        cmbpersona1.getSelectionModel().select(-1);
        lat = 0;
        lon = 0;
        fechaCita.setValue(null);
        txtlugar.setText("");
    }

    public void clearMat() {
        cmbpersona22.getItems().clear();
        cmbpersona21.getSelectionModel().select(-1);

        fechaBoda.setValue(null);
        img1.setImage(null);
        img2.setImage(null);

    }

    @FXML
    void visualizarCita(MouseEvent event) {
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            if (event.getClickCount() == 2) {
                Cita c = tablabajas21.getSelectionModel().getSelectedItem().getValue();
                Llamada.verMapa(c.getNombreLugar(), c.getLugarAlt(), c.getLugarLat());
            }
        }
    }

    @FXML
    void altaMatrimonio(ActionEvent event) throws ParseException {
        Persona p1 = cmbpersona21.getSelectionModel().getSelectedItem();
        Persona p2 = cmbpersona22.getSelectionModel().getSelectedItem();
        if (fechaBoda.getValue() != null) {
            Matrimonio m = new Matrimonio(p1, p2, new SimpleDateFormat("yyyy-MM-dd").parse(fechaBoda.getValue().toString()));
            guardarModificar(m);
            p1.setCasado(true);
            p2.setCasado(true);
            guardarModificar(p1);
            guardarModificar(p2);
            cmbpersona1.getItems().remove(p1);
            cmbpersona1.getItems().remove(p2);
            cmbpersona21.getItems().remove(p1);
            cmbpersona21.getItems().remove(p2);
            anyadirMat(m);
            clearMat();
        }
    }

    @FXML
    void seleccionP1Mat(ActionEvent event) {
        Persona p = cmbpersona21.getSelectionModel().getSelectedItem();
        if (p != null) {
            System.out.println(p.isCasado());

            cmbpersona22.getItems().clear();
            img2.setImage(null);

            listaComboP22(p);
            if (p.getFoto() != null) {
                Image i = new Image(new ByteArrayInputStream(p.getFoto()));

                img1.setImage(i);
            } else {
                img1.setImage(null);
            }
        }
    }

    @FXML
    void seleccionP2Mat(ActionEvent event) {
        Persona p = cmbpersona22.getSelectionModel().getSelectedItem();
        if (p != null) {
            if (p.getFoto() != null) {
                Image i = new Image(new ByteArrayInputStream(p.getFoto()));

                img2.setImage(i);
            } else {
                img2.setImage(null);
            }
        }
    }

    @FXML
    void consultasclisexo(ActionEvent event) {
        String sexo = String.valueOf(cmbsexoconsulcli.getSelectionModel().getSelectedItem());
        if (sexo.equalsIgnoreCase("bisexual") || sexo.equalsIgnoreCase("Homosexual") || sexo.equalsIgnoreCase("Heterosexual")) {
            tablabajas2.getRoot().getChildren().clear();
            iniPersonasSexo(String.valueOf(sexo));
        } else {
            tablabajas.getRoot().getChildren().clear();
            tablabajas1.getRoot().getChildren().clear();
            tablabajas2.getRoot().getChildren().clear();
            cmbpersona1.getItems().clear();
            cmbpersona21.getItems().clear();
            iniPersonas();
        }
    }

    @FXML
    void enviaremail(ActionEvent event) {
        if (!lblmsotrarcita.getText().equalsIgnoreCase("")) {
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.getScene().lookup("#panelFondo").setEffect(frostEffect);
            Alerta.showAlertconforme(bundle.getString("sent"));
            stage.getScene().lookup("#panelFondo").setEffect(null);
        } else {
            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.getScene().lookup("#panelFondo").setEffect(frostEffect);
            Alerta.showAlert(bundle.getString("dateerr"));
            stage.getScene().lookup("#panelFondo").setEffect(null);
        }

    }

}
