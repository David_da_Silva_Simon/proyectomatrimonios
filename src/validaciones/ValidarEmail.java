package validaciones;

/**
 *
 * @author Grupo 2 (David Da Silva, Sergio Vazquez, Carlos Perez, David Grande)
 */
public class ValidarEmail {
    public static int validarmail(String email){
        int op=0;
        String validar="[\\w!#$%&’*+/=?`{|}~^-]+(?:\\.[\\w!#$%&’*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        if(!email.matches(validar))op=1;
        return op;
    }
}
