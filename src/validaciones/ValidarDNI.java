package validaciones;


import com.aeat.valida.Validador;

/**
 *
 * @author Grupo 2 (David Da Silva, Sergio Vazquez, Carlos Perez, David Grande)
 */
public class ValidarDNI {

    public static int comprobar(String dni) {
        Validador val = new Validador();
        int error = val.checkNif(dni);
        if (error > 0 && error < 20) {
            return 1;
        } else {
            return error;
        }

    }

}
