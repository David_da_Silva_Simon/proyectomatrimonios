
package proyectomat;

import java.io.File;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Grupo 2 (David Da Silva, Sergio Vazquez, Carlos Perez, David Grande)
 */
public class ProyectoMat extends Application {
    
    private double xOffset, yOffset;
    
    @Override
    public void start(Stage stage) throws Exception {
        URL url = new File("src/resources/Principal.fxml").toURL();
        ResourceBundle resources = ResourceBundle.getBundle("bundles/Matrimonios",
                            new Locale(System.getProperty("user.language"), System.getProperty("user.country")));
        if (resources==null){
            resources = ResourceBundle.getBundle("bundles/Matrimonios", new Locale("es"));
        }
        Parent root = FXMLLoader.load(url,resources);
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });
        Scene scene = new Scene(root);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        launch(args);
    }
    
}
